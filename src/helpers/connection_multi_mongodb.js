const mongoose = require('mongoose');
require('dotenv').config();

const newConnection = (uri) => {
  const conn = mongoose.createConnection(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });

  conn.on('connected', function () {
    console.log(`Mongodb ::: connected ::: ${this.name}`);
  });

  conn.on('disconnected', function () {
    console.log(`Mongodb ::: disconnected ::: ${this.name}`);
  });

  conn.on('error', function (error) {
    console.log(`Mongodb ::: error ::: ${JSON.stringify(error)}`);
  });

  return conn;
};

// Make test c
const URI_APP = process.env.URI_MONGODB_APP;
const URI_STORE = process.env.URI_MONGODB_STORE;

const appDBConnection = newConnection(URI_APP);
const storeDBConnection = newConnection(URI_STORE);

module.exports = {
  appDBConnection,
  storeDBConnection,
};
