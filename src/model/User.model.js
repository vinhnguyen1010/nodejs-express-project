const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt');
const { appDBConnection } = require('../helpers/connection_multi_mongodb');

const UserSchema = new Schema({
  email: {
    type: String,
    lowercase: true,
    unique: true,
    require: true,
  },

  password: {
    type: String,
    require: true,
  },
});

UserSchema.pre('save', async function (next) {
  try {
    console.log(`Called before save ::::`, this.email, this.password);
    const salt = await bcrypt.genSaltSync(10);
    const hashPassword = await bcrypt.hash(this.password, salt);
    this.password = hashPassword;
    next();
  } catch (error) {
    next(error);
  }
});

UserSchema.methods.isCheckPassword = async function (password) {
  try {
    return await bcrypt.compare(password, this.password);
  } catch (error) {
    return error;
  }
};

module.exports = appDBConnection.model('users', UserSchema);
