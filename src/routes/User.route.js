const express = require('express');

const { verifyAccessToken } = require('../helpers/jwt_service');
const UserController = require('../controllers/User.controller');

const route = express.Router();

route.post('/register', UserController.register);

route.post('/refresh-token', UserController.refreshToken);

route.post('/login', UserController.login);

route.delete('/logout', UserController.logout);

route.get('/getlists', verifyAccessToken, UserController.getlists);

module.exports = route;
