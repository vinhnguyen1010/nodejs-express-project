const User = require('../model/User.model');
const createError = require('http-errors');
const { userValidate } = require('../helpers/validation');
const { signAccessToken, signRefreshToken, verifyRefreshToken } = require('../helpers/jwt_service');
const client = require('../helpers/connection_redis');

module.exports = {
  //
  register: async (req, res, next) => {
    try {
      const { email, password } = req.body;

      const { error } = userValidate(req.body);

      console.log(`:::: error validate`, error);
      if (error) {
        throw createError(error.details[0].message);
      }

      const isExits = await User.findOne({
        email,
      });

      if (isExits) {
        throw createError.Conflict(`${email} is exists`);
      }

      const user = new User({
        email,
        password,
      });
      const saveUser = await user.save();

      return res.status(200).send({
        status: 200,
        data: saveUser,
      });
    } catch (error) {
      next(error);
    }
  },

  //
  refreshToken: async (req, res, next) => {
    try {
      console.log(req.body);
      const { refreshToken } = req.body;
      if (!refreshToken) throw createError.BadRequest();

      const { userId } = await verifyRefreshToken(refreshToken);
      console.log(`userId:::: ${userId}`);

      const accessToken = await signAccessToken(userId);
      const refToken = await signRefreshToken(userId);
      console.log('TCL ~ file: User.route.js ~ line 63 ~ refToken', refToken);

      return res.send({
        accessToken,
        refreshToken: refToken,
      });
    } catch (error) {
      next(error);
    }
  },

  //
  login: async (req, res, next) => {
    try {
      const { error } = userValidate(req.body);
      if (error) {
        throw createError(error.details[0].message);
      }

      const { email, password } = req.body;
      const user = await User.findOne({ email });

      if (!user) {
        throw createError.NotFound('User not regestered');
      }

      const isValid = await user.isCheckPassword(password);
      if (!isValid) {
        throw createError.Unauthorized();
      }

      const accessToken = await signAccessToken(user._id);
      const refreshToken = await signRefreshToken(user._id);

      res.send({
        accessToken,
        refreshToken,
      });
    } catch (error) {
      next(error);
    }
  },

  //
  logout: async (req, res, next) => {
    try {
      const { refreshToken } = req.body;
      if (!refreshToken) {
        throw createError.BadRequest();
      }
      const { userId } = await verifyRefreshToken(refreshToken);
      client.del(userId.toString(), (err, reply) => {
        if (err) {
          throw createError.InternalServerError();
        }
        res.send({ message: 'Logout' });
      });
    } catch (error) {
      next(error);
    }
  },

  //
  getlists: async (req, res, next) => {
    console.log('headers===', req.headers);

    const listUsers = [
      {
        email: 'abc@gmail.com',
      },
      {
        email: 'binhnasn@gmail.com',
      },
    ];

    res.json({
      listUsers,
    });
  },
};
