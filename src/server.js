const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const createError = require('http-errors');
const { use } = require('./routes/User.route');
const UserRoute = require('./routes/User.route');
require('dotenv').config();
const client = require('./helpers/connection_redis');

require('./helpers/connection_multi_mongodb');

client.set('foo', 'mern-app');
client.get('foo', (err, result) => {
  if (err) {
    throw createError.BadRequest();
  }

  console.log(result);
});

app.get('/', (req, res, next) => {
  res.send('Home Page');
});

// parse requests of content-type - application/json
app.use(bodyParser.json());
// app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use('/user', UserRoute);

app.use((req, res, next) => {
  next(createError.NotFound('This route does not exist'));
});

app.use((err, req, res, next) => {
  res.json({
    status: err.status || 500,
    message: err.message,
  });
});

const PORT = process.env.PORT || 8081;

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
